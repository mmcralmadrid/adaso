import unittest 
from app import app 

class TestFlaskApi(unittest.TestCase): 
    def setUp(self): 
        self.client = app.test_client() 
        self.client.testing = True 

    def test_get_root(self): 
        response = self.client.get('/') 
        data = response.get_data(as_text=True)   
        self.assertEqual(response.status_code, 200) 
        self.assertEqual(data, 'Hello, Félix!') 

if __name__ == "__main__": 
    unittest.main() 
